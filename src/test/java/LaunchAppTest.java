import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageClass.GoogleForm;

public class LaunchAppTest extends BaseTest {


    GoogleForm googleFormObj;

    @BeforeClass
    public void preClass() {
        googleFormObj = new GoogleForm();
    }

    @Test
    public void googleFormTest() {
        googleFormObj
                .openTestForm()
                .emailAddressInputFillIn("denysApp@gmail.com")
                .dateBirthDataPickerSet("11.06.1991")
                .nameInputFillIn("Denys")
                .sexSelect("Мужской")
                .moodSetCheckbox("Плохо")
                .moodSetCheckbox("Супер")
                .moodSetCheckbox("Другое")
                .sendButtonClick()
                .formSendShouldHave();
    }
}
