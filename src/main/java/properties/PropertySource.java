package properties;

public enum PropertySource {

    GLOBAL("globalProp.properties");

    public String sourceFile;

    PropertySource(String sourceFile) {
        this.sourceFile = sourceFile;
    }
}
