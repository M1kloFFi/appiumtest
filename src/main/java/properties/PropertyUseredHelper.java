package properties;

public class PropertyUseredHelper {
    public static final String DEVICE_NAME = PropertyLoader.loadGlobalProperty("deviceName");
    public static final String PLATFORM = PropertyLoader.loadGlobalProperty("platform");
    public static final String PLATFORM_VERSION = PropertyLoader.loadGlobalProperty("version");
    public static final String BROWSER = PropertyLoader.loadGlobalProperty("browser");

    final public static String HUB_URL = "http://0.0.0.0:4723/wd/hub";

}
