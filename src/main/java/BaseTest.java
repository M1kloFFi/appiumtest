import capability.BrowserCapability;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeSuite;

import java.net.MalformedURLException;
import java.net.URL;

import static properties.PropertyUseredHelper.*;

public class BaseTest {
    @BeforeSuite
    public void launchAppTest() throws MalformedURLException, InterruptedException {
        Configuration.browser = "chrome";
        Configuration.timeout = 20000;
        WebDriverRunner.setWebDriver(new RemoteWebDriver(new URL(HUB_URL),new BrowserCapability().setBrouserCapability(DEVICE_NAME,PLATFORM,PLATFORM_VERSION,BROWSER)));
    }
}
