package capability;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BrowserCapability {
    private DesiredCapabilities caps;

    public DesiredCapabilities setBrouserCapability(String deviceName,
                                             String platform,
                                             String version,
                                             String browser) {
        caps = new DesiredCapabilities();
        caps.setCapability("deviceName", deviceName);
        caps.setCapability("platformName", platform);
        caps.setCapability(MobileCapabilityType.VERSION, version);
        caps.setCapability(CapabilityType.BROWSER_NAME, browser);
        return caps;
    }
}
