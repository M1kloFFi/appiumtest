package pageClass;

import com.codeborne.selenide.Condition;
import com.github.javafaker.Faker;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class GoogleForm {
    String testUrl = "https://docs.google.com/forms/d/e/1FAIpQLSdqT5F9_qhPDmJ4lfIH7buVkUvjf4LS9ODdqD7PYfVbfFTnpA/viewform";
    Faker faker = new Faker();

    By emailAddressInput = By.name("emailAddress");
    By dateBirthDataPicker = By.cssSelector("input[type='date']");
    By nameInput = By.cssSelector("[aria-label='Имя']");
    By sexDrop = By.className("quantumWizMenuPaperselectOptionList");
    //BLOCK - Как ваше настроение
    By otherAnswerInput = By.cssSelector("[aria-label='Другой ответ']");
    By sendButton = By.className("quantumWizButtonPaperbuttonLabel");

    //[aria-label = 'Другое:']
    public GoogleForm openTestForm() {
        open(testUrl);
        return new GoogleForm();
    }

    public GoogleForm emailAddressInputFillIn(String emailText) {
        $(emailAddressInput).setValue(emailText);
        return this;
    }

    public GoogleForm dateBirthDataPickerSet(String data) {
        $(dateBirthDataPicker).sendKeys(data);
        return this;
    }

    public GoogleForm nameInputFillIn(String name) {
        $(nameInput).setValue(name);
        return this;
    }

    public GoogleForm sexSelect(String sex) {
        $(sexDrop).click();
        $(By.cssSelector(".exportSelectPopup [data-value='" + sex + "']")).click();
        return this;
    }

    public GoogleForm moodSetCheckbox(String checkBoxName) {
        if(checkBoxName.contains("Другое"))
            otherAnswerInputFillIn(faker.color().name());
        else $("[aria-label='" + checkBoxName + "']").click();
        return this;
    }

    public GoogleForm otherAnswerInputFillIn(String answer) {
        $(otherAnswerInput).setValue(answer);
        return this;
    }


    public GoogleForm sendButtonClick() {
        $(sendButton).click();
        return this;
    }

    public GoogleForm formSendShouldHave() {
        $(By.className("freebirdFormviewerViewResponsePageTitle")).shouldHave(Condition.text("Форма регистрации"));
        $(By.className("freebirdFormviewerViewResponseConfirmationMessage")).shouldHave(Condition.text("Ответ записан."));
        return this;
    }



}
